<!--9-->
<%-- 
    Document   : m_product-list
    Created on : Feb 5, 2024, 12:18:16 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Ashion Dashboard</title>
        <!-- Iconic Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="vendors/iconic-fonts/font-awesome/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" href="vendors/iconic-fonts/flat-icons/flaticon.css">
        <link rel="stylesheet" href="vendors/iconic-fonts/cryptocoins/cryptocoins.css">
        <link rel="stylesheet" href="vendors/iconic-fonts/cryptocoins/cryptocoins-colors.css">
        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- jQuery UI -->
        <link href="assets/css/jquery-ui.min.css" rel="stylesheet">
        <!-- Page Specific CSS (Slick Slider.css) -->
        <link href="assets/css/slick.css" rel="stylesheet">
        <link href="assets/css/datatables.min.css" rel="stylesheet">
        <!-- Foodtech styles -->
        <link href="assets/css/style.css" rel="stylesheet">
        <!-- Favicon -->
        <link rel="icon" type="image/png" sizes="32x32" href="favicon.ico">
        <!-- Boostrap web -->
        <style>
            .search {
                /* margin-right: 10px; */
                height: 100%;
                display: block;
                width: 100%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                appearance: none;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #dee2e6;
                border-radius: 5px;
                transition: .15s ease-in-out, box-shadow .15s ease-in-out;
            }

            .search:focus {
                /*border-color: #007bff;*/
                box-shadow: 0px 0px 4px 4px rgba(0, 0, 0, 0.1);
                /*padding: 10px;*/
            }

            .search--container {
                position: relative;
            }

            .search--container i {
                font-size: 1.3em;
                /*font-weight: 1800;*/
            }

            .search--icon {
                position: absolute;
                font-size: 1px;
                top: 30%;
                right: 15px;
                transform: translateY(-50%);
            }

            .dropdown-item:hover {
                background-color: rgb(177, 177, 177);
                /* Màu nền thay đổi khi hover */
                color: #ffffff;
                /* Màu chữ thay đổi khi hover */
            }

            .button--update {
                padding: 5px 5px;
                width: 100%;
                background-color: #007fff;
                border-color: #00ec8d;
                border-radius: 4px;
                text-align: center;
                /*height: 30px;*/
                align-items: center;



            }
            .button--update a{
                padding: 15px 5px;
                color: #fff;

            }
            .button--update:hover {
                color: white;
                background-color: #210094;
            }
            .table td {
                padding: 0.75rem;
                vertical-align: unset;
                border-top: 1px solid #dee2e6;
                /*text-align: center;*/
                overflow: hidden;

            }
            .table th {
                font-size: 17px;
                background-color: #F89406;
            }
            .long-text {
                width: 20%;
                overflow: hidden;

                white-space: pre-wrap;
                overflow-wrap: break-word;
            }
            .paging--fix {
                height: 170px;
            }

        </style>
        <script>
            function change() {
                document.getElementById("status").submit();
            }
            setTimeout(function () {
                document.getElementById("notification").style.display = "none"; // Ẩn thông báo sau 2 giây
                window.location.href = "ten_trang_chuyen_huong.jsp"; // Chuyển hướng trở lại màn hình người dùng
            }, 2000);
        </script>
    </head>

    <body class="ms-body ms-aside-left-open ms-primary-theme ms-has-quickbar">
        <!-- Sidebar Navigation Left Begin -->
        <aside id="ms-side-nav" class="side-nav fixed ms-aside-scrollable ms-aside-left">
            <!-- Logo -->
            <div class="logo-sn ms-d-block-lg">
                <a class="pl-0 ml-0 py-0 text-center" href="index.html">
                    <img style="max-width: 255px;" src="https://scontent.fsgn2-4.fna.fbcdn.net/v/t1.15752-9/423568413_220611387798284_6265991015599051178_n.png?_nc_cat=101&ccb=1-7&_nc_sid=8cd0a2&_nc_ohc=6xm1kK9w87wAX9TEvv_&_nc_ht=scontent.fsgn2-4.fna&oh=03_AdTwMqeyx_8lcqp09mOKsXlRojFzIL02Pvvb9Y3q1WDgZA&oe=65FA4050" alt="logo">
                </a>
            </div>
            <br>
            <!-- Navigation -->
            <ul class="accordion ms-main-aside fs-14" id="side-nav-accordion">
                <!-- Dashboard -->
                <li class="menu-item">
                    <a href="m_dashboard.jsp"> <span><i class="fas fa-columns" style="width: 14px; height: 14px;"></i>Dashboard</span>
                    </a>
                </li>
                <!-- /Dashboard -->

                <!-- Product Begin -->
                <li class="menu-item">
                    <a href="#" class="has-chevron" data-toggle="collapse" data-target="#product" aria-expanded="false" aria-controls="product1"> <span><i class="fa fa-archive fs-16"></i>Quản lý sản phẩm</span>
                    </a>
                    <ul id="product" class="collapse" aria-labelledby="product" data-parent="#side-nav-accordion">
                        <li> <a href="mkt_productlist">Danh sách sản phẩm</a>
                        </li>  
                        <li> <a href="mkt_addproduct">Thêm sản phẩm</a>
                        </li>
                        <li> <a href="pages/product/addproduct.html">Đánh giá sản phẩm</a>
                        </li>
                    </ul>
                </li>
                <!-- Product End -->

                <!-- News Begin -->
                <li class="menu-item">
                    <a href="#" class="has-chevron" data-toggle="collapse" data-target="#slider" aria-expanded="false" aria-controls="slider"> <span><i class="fas fa-newspaper"></i>Tin tức</span>
                    </a>
                    <ul id="slider" class="collapse" aria-labelledby="slider" data-parent="#side-nav-accordion">
                        <li> <a href="pages/invoice/invoicedetail.html">Bài đăng</a>
                        </li>
                        <li> <a href="mkt_sliderlist?cateNewsId=2" >Slider</a>
                        </li>
                        <li> <a href="mkt_addslider?cateNewsId=2" >Thêm Slider</a>
                        </li>
                    </ul>
                </li>
                <!-- News End -->

                <!-- Customer Begin -->
                <li class="menu-item">
                    <a href="pages/restaurants.html"> <span><i class="fas fa-user-friends fs-16"></i>Khách hàng</span>
                    </a>
                </li>
                <!-- Customer End -->



                <!-- ========= PROFILE ============================= -->
                <p style="border-bottom: 1px solid rgb(177, 177, 177); width: 80%; margin: 20px 22px"></p>
                <!-- Profile Begin -->
                <li class="menu-item">
                    <a href="sales.html"> <span><i class="fas fa-file-invoice" style="width: 14px; height: 14px;"></i>Chỉnh sửa thông tin</span>
                    </a>
                </li>
                <!-- Profile End -->

                <!-- change password -->
                <li class="menu-item">
                    <a href="sales.html"> <span><i class="fas fa-key" style="width: 14px; height: 14px;"></i>Đổi mật khẩu</span>
                    </a>
                </li>
                <!-- change password  -->

                <!-- Logout Begin -->
                <li class="menu-item">
                    <a href="sales.html"> <span><i class="fas fa-sign-out-alt" style="width: 14px; height: 14px;"></i>Đăng xuất</span>
                    </a>
                </li>
                <!-- Logout End -->


            </ul>
        </aside>
        <!-- Sidebar Navigation Left End -->


        <!-- Sidebar Right -->
        <aside id="ms-recent-activity" class="side-nav fixed ms-aside-right ms-scrollable">

            <div class="ms-aside-header">
                <ul class="nav nav-tabs tabs-bordered d-flex nav-justified mb-3" role="tablist">
                    <li class="fs-12"><a href="#activityLog" aria-controls="activityLog" class="active" role="tab" data-toggle="tab"> Activity Log</a></li>

                    <li><button type="button" class="close ms-toggler text-center" data-target="#ms-recent-activity" data-toggle="slideRight"><span aria-hidden="true">&times;</span></button></li>
                </ul>
            </div>


            <div class="ms-aside-body">

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active fade show" id="activityLog">
                        <ul class="ms-activity-log">
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-light">
                                    <i class="flaticon-gear"></i>
                                </div>
                                <h6>Update 1.0.0 Pushed</h6>
                                <span> <i class="material-icons">event</i>1 January, 2024</span>
                                <p class="fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, ula in sodales vehicula....</p>
                            </li>
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-success">
                                    <i class="flaticon-tick-inside-circle"></i>
                                </div>
                                <h6>Profile Updated</h6>
                                <span> <i class="material-icons">event</i>4 March, 2024</span>
                                <p class="fs-14">Curabitur purus sem, malesuada eu luctus eget, suscipit sed turpis. Nam pellentesque felis vitae justo accumsan, sed semper nisi sollicitudin...</p>
                            </li>
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-warning">
                                    <i class="flaticon-alert-1"></i>
                                </div>
                                <h6>Your payment is due</h6>
                                <span> <i class="material-icons">event</i>1 January, 2024</span>
                                <p class="fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, ula in sodales vehicula....</p>
                            </li>
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-danger">
                                    <i class="flaticon-alert"></i>
                                </div>
                                <h6>Database Error</h6>
                                <span> <i class="material-icons">event</i>4 March, 2024</span>
                                <p class="fs-14">Curabitur purus sem, malesuada eu luctus eget, suscipit sed turpis. Nam pellentesque felis vitae justo accumsan, sed semper nisi sollicitudin...</p>
                            </li>
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-info">
                                    <i class="flaticon-information"></i>
                                </div>
                                <h6>Checkout what's Trending</h6>
                                <span> <i class="material-icons">event</i>1 January, 2024</span>
                                <p class="fs-14">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, ula in sodales vehicula....</p>
                            </li>
                            <li>
                                <div class="ms-btn-icon btn-pill icon btn-secondary">
                                    <i class="flaticon-diamond"></i>
                                </div>
                                <h6>Your Dashboard is ready</h6>
                                <span> <i class="material-icons">event</i>4 March, 2024</span>
                                <p class="fs-14">Curabitur purus sem, malesuada eu luctus eget, suscipit sed turpis. Nam pellentesque felis vitae justo accumsan, sed semper nisi sollicitudin...</p>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-primary d-block"> View All </a>
                    </div>


                </div>

            </div>

        </aside>

        <!-- Main Content -->
        <main class="body-content">

            <!-- Navigation Bar -->
            <nav class="navbar ms-navbar">

                <div class="ms-aside-toggler ms-toggler pl-0" data-target="#ms-side-nav" data-toggle="slideLeft">
                    <span class="ms-toggler-bar bg-primary"></span>
                    <span class="ms-toggler-bar bg-primary"></span>
                    <span class="ms-toggler-bar bg-primary"></span>
                </div>
                <div style="width: 12%; padding: 10px 0;">
                    <img style="width: 100%;" src="https://scontent.fhan14-1.fna.fbcdn.net/v/t1.15752-9/415986971_1701360590387678_3307011220705221556_n.png?_nc_cat=107&ccb=1-7&_nc_sid=8cd0a2&_nc_ohc=tso1MuEDASYAX_AO5Iv&_nc_oc=AQkQFrorscwafEjAAlUbuJTSrmadsHMYl42q3djJWWUN-kzfDDkHp4EJsTQ3VBJRv3g&_nc_ht=scontent.fhan14-1.fna&oh=03_AdQx9-1PIA-_3gFwAiM_rPJiyS6SNrw7s79bvHsqHrA1yw&oe=65E4653C"
                         alt="">
                </div>


                <ul class="ms-nav-list ms-inline mb-0" id="ms-nav-options">
                    <li class="ms-nav-item ms-search-form pb-0 py-0">
                        <form class="ms-form" method="post">
                            <div class="ms-form-group my-0 mb-0 has-icon fs-14">
                                <input type="search" class="ms-form-input" name="search" placeholder="Search here..." value="">
                                <i class="flaticon-search text-disabled"></i>
                            </div>
                        </form>
                    </li>
                    <li class="ms-nav-item dropdown">
                        <a href="#" class="text-disabled ms-has-notification" id="mailDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-mail"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="mailDropdown">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Mail</span></h6><span class="badge badge-pill badge-success">3 New</span>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="ms-scrollable ms-dropdown-list">
                                <a class="media p-2" href="#">
                                    <div class="ms-chat-status ms-status-offline ms-chat-img mr-2 align-self-center">
                                        <img src="assets/img/foodtech/add-product-1.jpg" class="ms-img-round" alt="people">
                                    </div>
                                    <div class="media-body">
                                        <span>Hey man, looking forward to your new project.</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 30 seconds ago</p>
                                    </div>
                                </a>
                                <a class="media p-2" href="#">
                                    <div class="ms-chat-status ms-status-online ms-chat-img mr-2 align-self-center">
                                        <img src="assets/img/foodtech/add-product-1.jpg" class="ms-img-round" alt="people">
                                    </div>
                                    <div class="media-body">
                                        <span>Dear John, I was told you bought Foodtech! Send me your feedback</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 28 minutes ago</p>
                                    </div>
                                </a>
                                <a class="media p-2" href="#">
                                    <div class="ms-chat-status ms-status-offline ms-chat-img mr-2 align-self-center">
                                        <img src="assets/img/foodtech/add-product-1.jpg" class="ms-img-round" alt="people">
                                    </div>
                                    <div class="media-body">
                                        <span>How many people are we inviting to the dashboard?</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 6 hours ago</p>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-menu-footer text-center">
                                <a href="apps/email.html">Go to Inbox</a>
                            </li>
                        </ul>
                    </li>
                    <li class="ms-nav-item dropdown">
                        <a href="#" class="text-disabled ms-has-notification" id="notificationDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-bell"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="notificationDropdown">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Notifications</span></h6><span class="badge badge-pill badge-info">4 New</span>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="ms-scrollable ms-dropdown-list">
                                <a class="media p-2" href="#">
                                    <div class="media-body">
                                        <span>12 ways to improve your crypto dashboard</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 30 seconds ago</p>
                                    </div>
                                </a>
                                <a class="media p-2" href="#">
                                    <div class="media-body">
                                        <span>You have newly registered users</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 45 minutes ago</p>
                                    </div>
                                </a>
                                <a class="media p-2" href="#">
                                    <div class="media-body">
                                        <span>Your account was logged in from an unauthorized IP</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 2 hours ago</p>
                                    </div>
                                </a>
                                <a class="media p-2" href="#">
                                    <div class="media-body">
                                        <span>An application form has been submitted</span>
                                        <p class="fs-10 my-1 text-disabled"><i class="material-icons">access_time</i> 1 day ago</p>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-menu-footer text-center">
                                <a href="#">View all Notifications</a>
                            </li>
                        </ul>
                    </li>

                    <li class="ms-nav-item ms-nav-user dropdown">
                        <a href="#" id="userDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <img class="ms-user-img ms-img-round float-right" src="assets/img/foodtech/customer-6.jpg" alt="people"> </a>
                        <ul class="dropdown-menu dropdown-menu-right user-dropdown" aria-labelledby="userDropdown">
                            <li class="dropdown-menu-header">
                                <h6 class="dropdown-header ms-inline m-0"><span class="text-disabled">Welcome, Anny Farisha</span></h6>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="ms-dropdown-list">
                                <a class="media fs-14 p-2" href="prebuilt-pages/user-profile.html"> <span><i class="flaticon-user mr-2"></i> Profile</span> </a>
                                <a class="media fs-14 p-2" href="apps/email.html"> <span><i class="flaticon-mail mr-2"></i> Inbox</span> <span class="badge badge-pill badge-info">3</span> </a>
                                <a class="media fs-14 p-2" href="prebuilt-pages/user-profile.html"> <span><i class="flaticon-gear mr-2"></i> Account Settings</span> </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li class="dropdown-menu-footer">
                                <a class="media fs-14 p-2" href="prebuilt-pages/lock-screen.html"> <span><i class="flaticon-security mr-2"></i> Lock</span> </a>
                            </li>
                            <li class="dropdown-menu-footer">
                                <a class="media fs-14 p-2" href="prebuilt-pages/default-login.html"> <span><i class="flaticon-shut-down mr-2"></i> Logout</span> </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <div class="ms-toggler ms-d-block-sm pr-0 ms-nav-toggler" data-toggle="slideDown" data-target="#ms-nav-options">
                    <span class="ms-toggler-bar bg-primary"></span>
                    <span class="ms-toggler-bar bg-primary"></span>
                    <span class="ms-toggler-bar bg-primary"></span>
                </div>

            </nav>

            <!-- Product List Begin -->
            <div class="ms-content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Breadcrumb Begin -->
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb pl-0">
                                <li class="breadcrumb-item"><a href="m_dashboard.jsp"><i class="material-icons">home</i> Trang chủ</a></li>

                                <li class="breadcrumb-item active" aria-current="page">Danh sách sản phẩm</li>
                            </ol>
                        </nav>
                        <!-- Breadcrumb End -->


                        <!-- Table Product -->
                        <div class="ms-panel">
                            <div class="ms-panel-header">
                                <h6>Danh sách sản phẩm</h6>
                            </div>
                            <div class="ms-panel-body">
                                <div class="table-responsive">
                                    <div>

                                        <div class="d-flex justify-content-between  mx-2">

                                            <div class="dropdown" style="display: flex">
                                                <div>
                                                    <a class="btn btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Thể loại sản phẩm
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <c:forEach items="${requestScope.listCateProduct}" var="lcp">
                                                            <a class="dropdown-item" href="mkt_productlist?cateProId=${lcp.cateProId}">${lcp.cateProName}</a>  
                                                        </c:forEach>
                                                    </div>
                                                    <c:if test="${cateProId != null}">
                                                        <h5 class="my-3">Thể loại: ${categoryProduct.cateProName}</h5>
                                                    </c:if>
                                                </div>
                                                <div class="mx-3">
                                                    <a class="btn btn-info dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Trạng thái
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                        <a class="dropdown-item" href="mkt_productlist?cateProId=${cateProId}&proStatus=on&searching=${searching}" style="color: green;">Đang hoạt động</a>  
                                                        <a class="dropdown-item" href="mkt_productlist?cateProId=${cateProId}&proStatus=off&searching=${searching}" style="color: #838383;">Đã dừng</a>  

                                                    </div>

                                                </div>

                                                <div style="position: relative; top: 29px" class="mx-2">
                                                    <form id="status" action="mkt_productlist" method="get">
                                                        <inline>Sản phẩm xếp hạng cao</inline>
                                                        
                                                        <input type="radio" name="sortByRank" ${sortByRank eq 'sortByRank'?'checked':''} onchange="change()" value="sortByRank" />
                                                        <input type="hidden" name="proStatus" value="${proStatus}"/>
                                                        <input type="hidden" name="cateProId" value="${cateProId}"/>
                                                        <input type="hidden" name="searching" value="${searching}"/>
                                                        <input type="hidden" name="numberPage" value="${numberPage}" />
                                                    </form>

                                                </div>


                                            </div>


                                            <div class="search--container my-3">
                                                <form action="mkt_productlist" method="get">
                                                    <input type="text" name="searching" class="search" placeholder="Tìm kiếm..." />
                                                    
                                                    <!--<i class="search--icon bi bi-search"></i>-->
                                                </form>
                                            </div>

                                        </div>


                                        <table class="table table-hover w-100 table-red text-center">

                                            <thead>

                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Tên sản phẩm</th>
                                                    <th scope="col">Giá bán</th>
                                                    <th scope="col">Giá gốc</th>
                                                    <th scope="col">Xếp hạng</th>
                                                    <th scope="col">Đã bán</th>
                                                    <th scope="col">Ngày tạo sản phẩm</th>
                                                    <th scope="col">Trạng thái</th>
                                                    <th scope="col">Xem chi tiết</th>


                                                </tr>
                                            </thead>
                                            <tbody>
                                            <div>
                                                <h5 style="text-align: center">${msgNotFound}</h5>
                                                <h5 style="text-align: center">${msgFound}</h5>
                                            </div>
                                            <%
                                                String cateProId = request.getParameter("cateProId");
                                                if (cateProId == null) {
                                                    cateProId = null;
                                                    request.setAttribute("cateProId", cateProId);
                                                }
                                            %>
                                            <% int cnt = (Integer) request.getAttribute("startNumOr");                                        
                                            %>
                                            <c:forEach items="${requestScope.listProduct}" var="lp">
                                                <tr class="my-5">
                                                    <% cnt++; %>
                                                    <td class="font-weight-bold"><%= cnt %></td>
                                                    <td class="long-text">${lp.proName}</td>
                                                    <fmt:formatNumber value="${lp.proPrice}" pattern="###,###" var="price"/>
                                                    <td>${price}đ</td>
                                                    <fmt:formatNumber value="${lp.proCost}" pattern="###,###" var="cost"/>           
                                                    <td>${cost}đ</td>
                                                    <td>${lp.proRating}</td>
                                                    <td>${lp.proSold}</td>
                                                    <td>${lp.proCreatedDate}</td>
                                                    <c:set var="check" value="on" />
                                                    <c:choose>  
                                                        <c:when test="${lp.proStatus == check}">
                                                            <td style="color: green; font-weight: bold;">Hoạt động</td>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <td style="color: #838383; font-weight: bold;">Đã dừng</td>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <td class="text-center">

                                                        <span>
                                                            <a href="mkt_productdetail?productid=${lp.proId}">
                                                                <i class="fas fa-eye" style="color: #b1a9a9; font-size: 14px;"></i>
                                                            </a>
                                                        </span>

                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>

                                        <nav aria-label="..." class="paging--fix">
                                            <ul class="pagination justify-content-lg-end mt-5">
                                                <c:choose>
                                                    <c:when test="${numberPage == 1}">
                                                        <li class="page-item disabled">
                                                            <a class="page-link" href="#" tabindex="-1">Trang trước</a>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="page-item ${paging eq "previous" && numberPage != 3 && numberPage != 2?"active":""}">
                                                            <a class="page-link" href="mkt_productlist?numberPage=${(numberPage - 1)}&paging=previous&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">Trang trước</a>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:if test="${endPage == 0}">
                                                    <c:set var="endPage" value="${endPage + 1}" />
                                                </c:if>
                                                <c:if test="${endPage > 0}">
                                                    <li class="page-item ${numberPage == 1?"active":""}">
                                                        <a class="page-link" href="mkt_productlist?numberPage=1&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">1</a>
                                                    </li>
                                                </c:if>

                                                <c:if test="${endPage > 1}">
                                                    <li class="page-item ${numberPage == 2?"active":""}">
                                                        <a class="page-link" href="mkt_productlist?numberPage=2&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">2</a>
                                                    </li>
                                                </c:if>
                                                <c:if test="${endPage > 2}">

                                                    <li class="page-item ${numberPage == 3?"active":""}">

                                                        <a class="page-link" href="mkt_productlist?numberPage=3&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">3</a>
                                                    </li>
                                                </c:if>


                                                <c:if test="${numberPage > 3}">


                                                    <div style="position: relative; top: 20px; margin: 0 5px;">...</div>
                                                    <li class="page-item ${numberPage == numberPage?"active":""}">
                                                        <a class="page-link" href="mkt_productlist?numberPage=${numberPage}&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">${numberPage}</a>
                                                    </li>
                                                </c:if>
                                                <c:choose>
                                                    <c:when test="${numberPage == endPage}">
                                                        <li class="page-item disabled">
                                                            <a class="page-link" href="#" tabindex="-1">Trang sau</a>
                                                        </li>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="page-item">
                                                            <a class="page-link" href="mkt_productlist?numberPage=${(numberPage + 1)}&paging=next&cateProId=${cateProId}&proStatus=${proStatus}&sortByRank=${sortByRank}&searching=${searching}">Trang sau</a>
                                                        </li>
                                                    </c:otherwise>
                                                </c:choose>
                                            </ul>
                                            <h6>Hiển thị trang ${numberPage} trên ${endPage} trang</h6>
                                        </nav>
                                        <hr style="font-weight: 900;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Table Product -->

                    </div>

                </div>
            </div>
            <!-- Product List End -->



        </main>

        <!-- SCRIPTS -->
        <!-- Global Required Scripts Start -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/perfect-scrollbar.js">
        </script>
        <script src="assets/js/jquery-ui.min.js">
        </script>
        <!-- Global Required Scripts End -->
        <!-- Page Specific Scripts Start -->

        <script src="assets/js/Chart.bundle.min.js">
        </script>
        <script src="assets/js/widgets.js">
        </script>
        <script src="assets/js/clients.js">
        </script>
        <script src="assets/js/Chart.Financial.js">
        </script>
        <script src="assets/js/d3.v3.min.js">
        </script>
        <script src="assets/js/topojson.v1.min.js">
        </script>
        <script src="assets/js/datatables.min.js">
        </script>
        <script src="assets/js/data-tables.js">
        </script>
        <!-- Page Specific Scripts Finish -->
        <!-- Foodtech core JavaScript -->
        <script src="assets/js/framework.js"></script>
        <!-- Settings -->
        <script src="assets/js/settings.js"></script>

    </body>


</html>
