/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.team3.onlineshopping.controllerSalesman;

import com.team3.onlineshopping.dal.AccountDAO;
import com.team3.onlineshopping.dal.AddressDAO;
import com.team3.onlineshopping.dal.OrderDAO;
import com.team3.onlineshopping.dal.OrderDetailsDAO;
import com.team3.onlineshopping.model.Account;
import com.team3.onlineshopping.model.Address;
import com.team3.onlineshopping.model.Order;
import com.team3.onlineshopping.model.OrderDetails;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author PC
 */
public class SaleOrderDetailsServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SaleOrderDetailsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SaleOrderDetailsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        String type = request.getParameter("type");
        String index = request.getParameter("index");

        OrderDAO or_dao = new OrderDAO();
        AccountDAO acc_dao = new AccountDAO();
        AddressDAO add_dao = new AddressDAO();
        OrderDetailsDAO orde_deo = new OrderDetailsDAO();

        Order order = or_dao.getById(orderId);
        Account acc = acc_dao.getAccByCusId(order.getCusId());
        Address add = add_dao.getAddByOrderId(orderId);
        List<OrderDetails> orderDetails = orde_deo.getByOrderId(orderId);

        String status = "";
        if (order.getOrStatus().equalsIgnoreCase("delivered")) {
            status = "Đã giao hàng";
        } else if (order.getOrStatus().equalsIgnoreCase("cancelled")) {
            status = "Đã hủy";
        } else if (order.getOrStatus().equalsIgnoreCase("pending")) {
            status = "Đang giao hàng";
        } else if (order.getOrStatus().equalsIgnoreCase("delivering")) {
            status = "Chờ xử lý";
        }

        request.setAttribute("status", status);
        request.setAttribute("order", order);
        request.setAttribute("customer", acc);
        request.setAttribute("address", add);
        request.setAttribute("type", type);
        request.setAttribute("pageNumber", index);
        request.setAttribute("orderDetails", orderDetails);

        request.getRequestDispatcher("s_orderdetails.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int orderId = Integer.parseInt(request.getParameter("orderId"));
        String status = request.getParameter("status");
        String type = request.getParameter("type");

        OrderDAO or_dao = new OrderDAO();

        Order order = or_dao.getById(orderId);
        order.setOrStatus(status);
        or_dao.update(order);

        response.sendRedirect("sale_orderdetails?orderId=" + orderId);
    }

}
