/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.team3.onlineshopping.information;

public class HtmlContent {
    public static String resetPassword(String resetPassLink){
        String html = "<!DOCTYPE html>\n"
                    + "<html lang=\"en\">\n"
                    + "\n"
                    + "<head>\n"
                    + "    <meta charset=\"UTF-8\">\n"
                    + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
                    + "</head>\n"
                    + "\n"
                    + "<body style=\"font-family: Arial, Helvetica, sans-serif;\">\n"
                    + "    <div class=\"image\" style=\"width: 10rem;\">\n"
                    + "        <img src=\"https://scontent.fhan14-1.fna.fbcdn.net/v/t1.15752-9/415986971_1701360590387678_3307011220705221556_n.png?_nc_cat=107&ccb=1-7&_nc_sid=8cd0a2&_nc_ohc=V2ZeUHI0viMAX-obv7t&_nc_oc=AQk00jCeUCuEPPd8XcE6oQs470-WNJk7DuFHo_dkl9lGsNgcp2dVsUo09mp_a5i8vgk&_nc_ht=scontent.fhan14-1.fna&oh=03_AdTSM_Cyy8H20BDq1THFcSkV3Eu-2-NLOAj0BMfPoy_lOA&oe=65D0D6FC\"\n"
                    + "            alt=\"\" style=\"width: 100%;\">\n"
                    + "    </div>\n"
                    + "    <div class=\"container\">\n"
                    + "        <h1>Thiết lập lại mật khẩu</h1>\n"
                    + "        <p>Bạn đã yêu cầu thiết lập lại mật khẩu. <br>\n"
                    + "            Vui lòng thiết lập lại mật khẩu của mình bằng cách click <strong>\"Đặt lại mật\n"
                    + "                khẩu\"</strong> bên dưới:</p><br>\n"
                    + "        <a href=" + resetPassLink + " style=\"text-decoration: none;\">\n"
                    + "            <p style=\"width: 10rem;background-color: rgb(22, 22, 175);\n"
                    + "            color: #FFF;text-align: center;padding: 10px 5px;\n"
                    + "            font-weight: bold;border-radius: 2px;\">Đặt lại mật khẩu</p>\n"
                    + "        </a><br>\n"
                    + "        <i style=\"color: rgb(255, 0, 0);\">(*) Lưu ý: Mã kích hoạt chỉ có hiệu lực trong vòng 5 phút</i>\n"
                    + "        <br><br><br>\n"
                    + "        <p>Trân trọng,</p>\n"
                    + "        <p>Ashion</p>\n"
                    + "    </div>\n"
                    + "</body>\n"
                    + "\n"
                    + "</html>";
        
        return html;
    }
}
