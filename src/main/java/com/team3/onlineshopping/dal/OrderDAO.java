/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.team3.onlineshopping.dal;

import com.team3.onlineshopping.model.Order;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
public class OrderDAO extends DBContext implements IDAO<Order> {

    @Override
    public List<Order> getAll() {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT * FROM `Order` ";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            ResultSet rs = st.executeQuery();
            //loop until select the last object
            while (rs.next()) {
                Order orders = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                list.add(orders);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    @Override
    public void add(Order t) {
        String sql = "INSERT INTO `Order` (OrderTitle, OrderDate, OrderTotalPrice, OrderStatus, EmployeeId, CustomerId,AddressId)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, t.getOrTitle());
            st.setString(2, t.getOrDate());
            st.setDouble(3, t.getOrTotalPrice());
            st.setString(4, t.getOrStatus());
            st.setInt(5, t.getEmId());
            st.setInt(6, t.getCusId());
            st.setInt(7, t.getAddId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    @Override
    public void update(Order t) {
        String sql = "UPDATE `Order`\n"
                + "   SET OrderTitle = ?\n"
                + "      ,OrderDate = ?\n"
                + "      ,OrderTotalPrice = ?\n"
                + "      ,OrderStatus = ?\n"
                + "      ,EmployeeId = ?\n"
                + "      ,CustomerId = ?\n"
                + "      ,AddressId = ?\n"
                + " WHERE OrderId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, t.getOrTitle());
            st.setString(2, t.getOrDate());
            st.setDouble(3, t.getOrTotalPrice());
            st.setString(4, t.getOrStatus());
            st.setInt(5, t.getEmId());
            st.setInt(6, t.getCusId());
            st.setInt(7, t.getAddId());
            st.setInt(8, t.getOrId());

            int rs = st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM `Order`\n"
                + " WHERE OrderId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);

            int rs = st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Order getById(int id) {
        String sql = "SELECT * FROM `Order` Where OrderId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            // loop until select the last object
            if (rs.next()) {
                Order u = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                return u;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void changeStatus(int id, String status) {
        String sql = "UPDATE `Order` \n"
                + "   SET OrderStatus = ?\n"
                + " WHERE OrderId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, status);
            st.setInt(2, id);

            int rs = st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    //AnhTV
    public int getTotalOrder() {
        String sql = "SELECT COUNT(*) FROM `order`";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            // loop until select the last object
            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int getTotalOrder(String status) {
        String sql = "SELECT COUNT(*) FROM `order`"
                + "WHERE OrderStatus = '" + status + "'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            // loop until select the last object
            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int getTotalOrder(String tilte, String name, String status, String address, String beginDate, String endDate, String beginPrice, String endPrice) {
        String sql = "SELECT COUNT(*) FROM `order` AS ord\n"
                + "INNER JOIN Customer AS cus ON cus.CustomerId = ord.CustomerId\n"
                + "INNER JOIN Account AS acc ON acc.AccountId = cus.AccountId\n"
                + "INNER JOIN Address AS ad ON ad.AddressId = ord.AddressId\n";
        if (tilte != null) {
            sql += "WHERE OrderTitle LIKE '%" + tilte + "%' \n";
        }
        if (name != null) {
            sql += "AND AccountName LIKE '%" + name + "%'\n";
        }
        if (status != null && !"all".equals(status)) {
            sql += "AND OrderStatus LIKE '%" + status + "%'\n";
        }
        if (address != null) {
            sql += "AND AddressName LIKE '%" + address + "%'\n";
        }
        if (beginDate != null && endDate != null) {
            sql += "AND OrderDate BETWEEN '" + beginDate + "' AND '" + endDate + "'\n";
        }
        if (beginPrice != null && endPrice != null) {
            sql += "AND OrderTotalPrice BETWEEN " + beginPrice + " AND " + endPrice + "\n";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            // loop until select the last object
            if (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    public List<Order> getAll(int index) {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT * FROM `order`\n"
                + "ORDER BY OrderDate DESC, OrderId DESC \n"
                + "LIMIT 10 OFFSET ?;";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            //loop until select the last object
            while (rs.next()) {
                Order orders = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                list.add(orders);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public Order getOrderLatest() {
        String sql = "SELECT * FROM `order`\n"
                + "ORDER BY OrderId DESC LIMIT 1";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            ResultSet rs = st.executeQuery();
            //loop until select the last object
            while (rs.next()) {
                Order orders = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                return orders;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public List<Order> getAll(String status, int index) {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT * FROM `order`\n"
                + "WHERE OrderStatus = ?"
                + "ORDER BY OrderDate DESC, OrderId DESC\n"
                + "LIMIT 10 OFFSET ?;";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setString(1, status);
            st.setInt(2, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            //loop until select the last object
            while (rs.next()) {
                Order orders = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                list.add(orders);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    public List<Order> FilterOrder(String tilte, String name, String status, String address, String beginDate, String endDate, String beginPrice, String endPrice, int index) {
        List<Order> list = new ArrayList<>();
        String sql = "SELECT distinct * FROM `order` AS ord\n"
                + "INNER JOIN Customer AS cus ON cus.CustomerId = ord.CustomerId\n"
                + "INNER JOIN Account AS acc ON acc.AccountId = cus.AccountId\n"
                + "INNER JOIN Address AS ad ON ad.AddressId = ord.AddressId\n";
        if (tilte != null) {
            sql += "WHERE OrderTitle LIKE '%" + tilte + "%' \n";
        }
        if (name != null) {
            sql += "AND AccountName LIKE '%" + name + "%'\n";
        }
        if (status != null && !"all".equals(status)) {
            sql += "AND OrderStatus LIKE '%" + status + "%'\n";
        }
        if (address != null) {
            sql += "AND AddressName LIKE '%" + address + "%'\n";
        }
        if (beginDate != null && endDate != null) {
            sql += "AND OrderDate BETWEEN '" + beginDate + "' AND '" + endDate + "'\n";
        }
        if (beginPrice != null && endPrice != null) {
            sql += "AND OrderTotalPrice BETWEEN " + beginPrice + " AND " + endPrice + "\n";
        }
        sql += "ORDER BY OrderDate DESC, OrderId DESC LIMIT 10 OFFSET ?;";
        try {
            PreparedStatement st = connection.prepareCall(sql);
            st.setInt(1, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            //loop until select the last object
            while (rs.next()) {
                Order orders = new Order(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getInt(6),
                        rs.getInt(7),
                        rs.getInt(8));
                list.add(orders);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    

    public static void main(String[] args) {
        OrderDAO a = new OrderDAO();
        System.out.println(a.getAll(1).size());

        List<String> list = new ArrayList<>();
        list.add("a");
//        list.add("b");


//        Order t = new Order(10, "Đơn Hàng Của Thái", "2024-01-09", 350000, "on", 5, 1, 5);
//        a.update(t);
//        a.delete(10);
//        Order acc = new Order(0, "aaaa", "2022-12-10", 300000, "off", 5, 1, 5);
//        a.add(acc);
//        System.out.println(a.getById(6).getOrTotalPrice());
//        a.changeStatus(1,"pending");
//        System.out.println(a.getTotalOrder("delivered"));
//        List<Order> list = a.FilterOrder(null, null, "all", null, null, null, null, null, 1);
//        for (Order order : list) {
//            System.out.println(order.getOrTitle() + "  " + order.getOrId());
//        }
//        System.out.println(a.getTotalOrder(null, null, "all", null, null, null, null, null));
    }

}
